import { random } from 'lodash';
import roomDatabase from '../db/rooms';
import { getGreetingQuote,
  getParticipantsQuote,
  secondsSpentForRace,
  getUsersProgressQuote,
  isUserNearFinish
} from '../helpers/commentatorHelper';
import { SECONDS_SCHEDULED_ANNOUNCE_INTERVAL,
  SECONDS_SCHEDULED_JOKE_INTERVAL } from '../config';

const activeCommentator = new Map();

const addCommentator = (roomId, commentator) => activeCommentator.set(roomId, commentator);
const getActiveCommentator = roomId => activeCommentator.get(roomId);

const deleteCommentator = roomId => {
  const commentator = activeCommentator.get(roomId);

  if (commentator) {
    const { scheduledAnnounceInterval, jokesInterval } = commentator;
    clearInterval(scheduledAnnounceInterval);
    clearInterval(jokesInterval);
    activeCommentator.delete(roomId);
  }
};

const updateScheduledAnnounceTime = roomId => {
  const commentator = activeCommentator.get(roomId);
  addCommentator(roomId, {
    ...commentator,
    previousAnnounce: Date.now(),
    nextScheduledAnnounceTime: (Date.now() + (SECONDS_SCHEDULED_ANNOUNCE_INTERVAL * 1000))
  });
};

const updatePreviousAnnounceTime = roomId => {
  const commentator = getActiveCommentator(roomId);
  addCommentator(roomId, {
    ...commentator,
    previousAnnounce: Date.now()
  });
};

const sendToRoom = (roomId, commentString) => {
  const { socket } = getActiveCommentator(roomId);
  socket.to(roomId).emit('COMMENTATOR_QUOTE', commentString);
  socket.emit('COMMENTATOR_QUOTE', commentString);
};

const sendAndUpdateTime = (roomId, commentStaring) => {
  updatePreviousAnnounceTime(roomId);
  sendToRoom(roomId, commentStaring);
};

function announceUsers(roomId) {
  const room = roomDatabase.getRoomById(roomId);
  if (room) {
    const { users } = room;
    const quote = getParticipantsQuote(users);
    sendAndUpdateTime(roomId, quote);
  }
}

function announceUsersProgress(roomId) {
  const { users } = roomDatabase.getRoomById(roomId);
  const toSend = getUsersProgressQuote(users);
  sendToRoom(roomId, toSend);
  updateScheduledAnnounceTime(roomId);
}

const checkTimeBetweenAnnounce = (previousAnnounce, nextAnnounce) => {
  const now = Date.now();
  return (now - previousAnnounce > 5000 && nextAnnounce - now > 5000);
};

function fillTimeBetweenAnnounce(roomId) {
  const { previousAnnounce, nextScheduledAnnounceTime, jokes } = getActiveCommentator(roomId);
  if (checkTimeBetweenAnnounce(previousAnnounce, nextScheduledAnnounceTime)) {
    sendAndUpdateTime(roomId, jokes[random(0, (jokes.length - 1))]);
  }
}

// TODO: add to functional
// eslint-disable-next-line
function endGameNear(roomId) {
  const { gameStartedAt } = roomDatabase.getRoomById(roomId);
  const timeLeft = Math.round((Date.now() - gameStartedAt) / 1000);
  if (timeLeft <= 10) {
    sendAndUpdateTime(roomId, `До конца игры осталось ${timeLeft}сек.`);
  }
}

function userNearFinish(room, userId) {
  const { username } = room.users.find(user => user.id === userId);
  const { id: roomId } = room;
  const quote = `${username} приближается к финишу.`;
  sendAndUpdateTime(roomId, quote);
}

function userFinished(room, userId) {
  const { username } = room.users.find(user => user.id === userId);
  const { id: roomId } = room;
  const quote = `Финишную прямую пересекает ${username}.`;
  sendAndUpdateTime(roomId, quote);
}

const filterFinishedUsers = users => users.filter(user => user.finished);

const sortFinishTimeDesc = (userA, userB) => userA.finished - userB.finished;

const mapUsersWithTime = (gameStartedAt, users) => (users
  .map(user => ([user.username, secondsSpentForRace(user.finished, gameStartedAt)]))
);

function gameOver(roomId) {
  const { gameStartedAt, users } = roomDatabase.getRoomById(roomId);
  const finishedUsers = filterFinishedUsers(users);
  const gameTimeIsOver = !!(finishedUsers.length < users.length);

  const sortedUsers = finishedUsers.sort(sortFinishTimeDesc);
  const topThreeUsers = mapUsersWithTime(gameStartedAt, sortedUsers).slice(0, 3);

  let finalQuote = '';

  switch (true) {
    case (!topThreeUsers.length): {
      finalQuote = 'Никто не добрался до финиша :\'(';
      break;
    }
    case (topThreeUsers.length === 1): {
      const [username, userTimeSpent] = topThreeUsers[0];
      finalQuote = `Финишировал только ${username} с результатом ${userTimeSpent}сек.`;
      break;
    }
    default: {
      const places = ['Первое место занимает ', ', на втором месте ', ', третье место досталось '];
      const usersPlaces = topThreeUsers.map((user, index) => {
        const [username, userTimeSpent] = user;
        return `${places[index]} ${username} с результатом ${userTimeSpent}cек.`;
      });

      const { username: lastUsername } = sortedUsers[sortedUsers.length - 1];
      const gameOverQuote = gameTimeIsOver
        ? 'Время вышло. И финальный результат:\n'
        : `Финишную прямую пересёк ${lastUsername}. И финальный результат:\n`;

      finalQuote = gameOverQuote + usersPlaces.join(' ');
    }
  }
  sendToRoom(roomId, finalQuote);
  deleteCommentator(roomId);
}

// can it be like a Facade?
function progress(room, userId) {
  if (!room) {
    return;
  }

  const user = room.users.find(user => user.id === userId);
  const { currentCharacter, totalCharacters } = user?.progress;

  if (isUserNearFinish(currentCharacter, totalCharacters)) {
    userNearFinish(room, userId);
  }
  if (currentCharacter === totalCharacters) {
    userFinished(room, userId);
  }
}

// eslint-disable-next-line
function commentator(roomId, socket, jokes) {
  setTimeout(announceUsers, 7000, roomId);
  const scheduledAnnounceProgress = setInterval(
    announceUsersProgress, (SECONDS_SCHEDULED_ANNOUNCE_INTERVAL * 1000), roomId
  );

  const now = Date.now();
  const jokesInterval = setInterval(
    fillTimeBetweenAnnounce, (SECONDS_SCHEDULED_JOKE_INTERVAL * 1000), roomId
  );

  addCommentator(roomId, {
    roomId,
    socket,
    jokes,
    jokesInterval,
    nextScheduledAnnounceTime: (now + (SECONDS_SCHEDULED_ANNOUNCE_INTERVAL * 1000)),
    scheduledAnnounceInterval: scheduledAnnounceProgress,
    previousAnnounce: now
  });

  const toSend = getGreetingQuote();
  sendToRoom(roomId, toSend);
}

// can it even be considered as factory?
function createCommentator(roomId, socket, type) {
  switch (type) {
    case 'programming': {
      /* eslint-disable */
      const jokes = ["“Debugging” is like being the detective in a crime drama where you are also the murderer.",
        "How do you tell HTML from HTML5?\n- Try it out in Internet Explorer\n- Did it work?\n- No?\n- It's HTML5.",
        "The six stages of debugging:\n1. That can't happen.\n2. That doesn't happen on my machine.\n3. That shouldn't happen.\n4. Why does that happen?\n5. Oh, I see.\n6. How did that ever work?",
        "programming joke #4",
        "programming joke #5",
      ];
      commentator(roomId, socket, jokes);
      break;
    }
    default: {
      const jokes = ["default joke #1",
        "I'm reading a book about anti-gravity. It's impossible to put down!",
        "default joke #3",
        "default joke #4",
        "default joke #5"
      ];
      /* eslint-enable */
      commentator(roomId, socket, jokes);
    }
  }
}

export default {
  createCommentator,
  deleteCommentator,
  gameOver,
  progress
};
