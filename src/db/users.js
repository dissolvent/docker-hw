const users = [];

function addUser(id, username) {
  const user = { id, username };
  users.push(user);
  return user;
}

function getUserByUsername(username) {
  return users.find(user => user.username === username);
}

function getUserById(id) {
  return users.find(user => user.id === id);
}

function deleteUser(id) {
  const index = users.findIndex(user => user.id === id);
  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
}

export default {
  addUser,
  getUserByUsername,
  getUserById,
  deleteUser
};
