/* eslint-disable */
import {
  updateRooms,
  createNewRoom,
  joinRoom,
  leaveRoom,
  updateRoom
} from './services/roomsService.mjs';
import {
  startCountdown,
  startGame,
  stopGame,
  updateCountdownEnd,
  updateCommentatorQuote
} from './services/gameService.mjs';
/* eslint-enable */

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

// eslint-disable-next-line
const socket = io('/game', { query: { username } });

const login = () => {
  alert('Username already exists. Please try another name.');
  sessionStorage.clear('username');
  window.location.replace('/login');
};

const createRoomBtn = document.getElementById('create-new-room');
createRoomBtn.addEventListener('click', () => createNewRoom(socket));

socket.on('USER_EXISTS', login);
socket.on('ROOM_EXISTS', () => alert('Room already exists'));
socket.on('UPDATE_ROOMS', rooms => updateRooms(socket, rooms));
socket.on('JOIN_ROOM_DONE', room => joinRoom(socket, room));
socket.on('LEAVE_ROOM_DONE', leaveRoom);
socket.on('UPDATE_ROOM', room => updateRoom(room));
socket.on('GAME_COUNTDOWN_START', gameData => startCountdown(gameData, socket));
socket.on('TIME_UNTIL_START', time => startGame(time));
socket.on('TIME_UNTIL_END', time => updateCountdownEnd(time));
socket.on('GAME_ENDED', stopGame);
socket.on('COMMENTATOR_QUOTE', quote => updateCommentatorQuote(quote));
